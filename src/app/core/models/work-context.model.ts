import { User } from './user.model';
import {Project} from './project.model';
import {SubProject} from './sub-project.model';
import {DefaultSettings} from './default-settings.model';

export class WorkContext {
  user: User;
  identityUrl: string;
  project: Project;
  subProject: SubProject;
  defaultSettings: DefaultSettings;
}

