import {ApiAction} from './api-action';

export class GetFolders extends ApiAction {
  constructor(subProjectId: string) {
    super('GetFolders');
    this.subProjectId = subProjectId;
  }

  subProjectId: string;
}

export class AddFolder extends ApiAction {
  subProjectId: string;
  title: string;
  descritpion: string;

  constructor(subProjectId: string, title: string, descritpion?: string) {
    super('AddFolder');
    this.subProjectId = subProjectId;
    this.title = title;
    this.descritpion = descritpion;
  }
}

export class GetProjects extends ApiAction {
  constructor() {
    super('GetProjects');
  }
}

export class AddProject extends ApiAction {
  constructor(title: string, description?: string) {
    super('AddProject');
    this.title = title;
    this.description = description;
  }

  title: string;
  description: string;
}

export class UpdateProject extends ApiAction {
  id: string;

  constructor(id: string, title: string, description?: string) {
    super('UpdateProject');
    this.title = title;
    this.description = description;
    this.id = id;
  }

  title: string;
  description: string;
}

export class DeleteProject extends ApiAction {
  constructor(id: string) {
    super('DeleteProject');
    this.id = id;
  }

  id: string;
}

export class DeleteFolder extends ApiAction {
  constructor(id: string) {
    super('DeleteFolder');
    this.id = id;
  }

  id: string;
}

export class AddSubProject extends ApiAction {
  constructor(projectId: string, title: string, description?: string) {
    super('AddSubProject');
    this.projectId = projectId;
    this.title = title;
    this.description = description;
  }

  projectId: string;
  title: string;
  description: string;
}

export class UpdateFolder extends ApiAction {
  constructor(title: string, id: string, description: string) {
    super('UpdateFolder');
    this.title = title;
    this.id = id;
    this.description = description;
  }

  title: string;
  id: string;
  description: string;
}

export class UpdateSubProject extends ApiAction {
  id: string;
  projectId: string;

  constructor(id: string, projectId: string, title: string, description?: string) {
    super('UpdateSubProject');
    this.title = title;
    this.description = description;
    this.id = id;
    this.projectId = projectId;
  }

  title: string;
  description: string;
}

export class DeleteSubProject extends ApiAction {
  constructor(id: string) {
    super('DeleteSubProject');
    this.id = id;
  }

  id: string;
}

export class GetSubProjects extends ApiAction {
  constructor(projectId: string) {
    super('GetSubProjects');
    this.projectId = projectId;
  }

  projectId: string;
}

export class GetSetting extends ApiAction {
  constructor(projectId: string, subProjectId: string) {
    super('GetSetting');
    this.projectId = projectId;
    this.subProjectId = subProjectId;
  }

  projectId: string;
  subProjectId: string;
}

export class AddDefaultSettings extends ApiAction {
  projectId: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;

  constructor(projectId: string, subProjectId: string, systemId: string, catalogId: string) {
    super('AddSettings');
    this.projectId = projectId;
    this.subProjectId = subProjectId;
    this.catalogId = catalogId;
    this.systemId = systemId;
  }
}

export class ChangeDefaultSettings extends ApiAction {
  id: string;
  projectId: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;

  constructor(id: string, projectId: string, subProjectId: string, systemId: string, catalogId: string) {
    super('UpdateSettings');
    this.id = id;
    this.projectId = projectId;
    this.subProjectId = subProjectId;
    this.catalogId = catalogId;
    this.systemId = systemId;
  }
}

export class GetStatus extends ApiAction {
  constructor() {
    super('GetStatus');
  }
}

export class SaveDefects extends ApiAction {
  formModel: FormData;

  constructor(
    formModel: FormData
  ) {
    super('SaveDefects');
    this.formModel = formModel;
  }
}

export class GetTypicalChanges extends ApiAction {
  constructor() {
    super('GetTypicalChanges');
  }
}

export class GetAcceptedTypicalChanges extends ApiAction {
  constructor() {
    super('GetAcceptedTypicalChanges');
  }
}

export class GetNumberOfTypicalChanges extends ApiAction {
  constructor() {
    super('GetNumberOfTypicalChanges');
  }
}

export class GetTypicalAttributeChangesById extends ApiAction {
  id: string;

  constructor(typicalId: string) {
    super('GetTypicalAttributeChangesById');
    this.id = typicalId;
  }
}

export class GetAcceptedTypicalAttributeChangesById extends ApiAction {
  id: string;

  constructor(typicalId: string) {
    super('GetAcceptedTypicalAttributeChangesById');
    this.id = typicalId;
  }
}

export class AcceptChanges extends ApiAction {
  id: string;
  markAsInvalid: boolean;

  constructor(typicalId: string, markAsInvalid: boolean) {
    super('AcceptChanges');
    this.id = typicalId;
    this.markAsInvalid = markAsInvalid;
  }
}

export class ResetSession extends ApiAction {
  sessionId: string;

  constructor(sessionId) {
    super('ResetSession');
    this.sessionId = sessionId;
  }
}
