export class ApiAction {
  apiActionName: string;

  constructor(apiActionName: string) {
    this.apiActionName = apiActionName;
  }
}
