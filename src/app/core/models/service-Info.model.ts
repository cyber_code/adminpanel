export class APIResponse{ 
    serviceInfo: ServiceInfo[];
    commandLog: CommandLog[];
    queryLog: QueryLog[];
}


export class ServiceInfo{
    title: string;
    active: boolean;
    info: string;
 }

 export class CommandLog{
     id: number;
     executedBy: string;
     calledFrom: string;
     name: string;
     json: string;
     resultName: string;
     resultJson: string;
     errorExceptionType: string;
     errorExceptionMessage: string;
     errorExceptionStackTrace: string;
     errorExceptionJson: string;
     started: string;                        // "started": "2019-05-14T10:34:50.9996212+00:00"
     ended: string;                         // "ended": "2019-05-14T10:34:50.9996212+00:00"
     elapsedMilliseconds: number;
     ipAddress: string;
 }

 export class QueryLog{
    id: number;
    queriedBy; null
    appName: string;
    calledFrom: string;
    name: string;
    json: {};
    resultName: null;
    resultJson: null;
    errorExceptionType: null;
    errorExceptionMessage: null;
    errorExceptionStackTrace: null;
    errorExceptionJson: null;
    started: string;                        
    ended: string;                         
    elapsedMilliseconds: number;
    ipAddress: string;
 }
