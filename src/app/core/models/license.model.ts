export class LicenseInfo {
  deviceId: string;
  isSingleTenant: boolean;
  licenceStartDate: string;
  licenceEndDate: string;
  licenceNumberOfUsers: number;
  licenceType: string;
}

export class LicenseStatus {
  status: string;
  message: string;
}
