export class User {
  id: string;
  username: string;
  name: string;
  lastName: string;
  email: string;
  role: string;
  tenantId: string;
  token?: string;
  permissionGroups: any;
}
