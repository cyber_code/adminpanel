export class SubProject {
  id: string;
  projectId: string;
  title: string;
  description?: string;
}
