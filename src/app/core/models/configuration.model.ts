export class Configuration {
  identityUrl: string;
  apiUrl: string;
  reportsUrl: string;
  hashId: string;
}
