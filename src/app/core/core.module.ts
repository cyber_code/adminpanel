import {NgModule} from '@angular/core';
import {CoreRoutingModule} from './core-routing.module';
import {LoginComponent} from './components/login/login.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {DialogComponent} from './components/dialog/dialog.component';
import {MessageComponent} from './components/message/message.component';
import {MessageService} from './services/message.service';
import {MainScreenComponent} from './components/main-screen/main-screen.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {MonitoringPageComponent} from './components/monitoring-page/monitoring-page.component';
import {MonitoringService} from './services/monitoring.service';
import {GridModule, SharedModule} from '@progress/kendo-angular-grid';
import {CommonModule} from '@angular/common';
import {LayoutModule} from '@progress/kendo-angular-layout';
import {HttpExecutorService} from './services/http-executor.service';
import {AvatarModule} from 'ngx-avatar';
import {NgChatModule} from 'ng-chat';
import {SettingsComponent} from '../body/components/settings/settings.component';
import {LicenseService} from './services/license.service';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [
    LoginComponent,
    DialogComponent,
    MessageComponent,
    DialogComponent,
    MainScreenComponent,
    NavbarComponent,
    SidebarComponent,
    MonitoringPageComponent,
    SettingsComponent,
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    CoreRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    GridModule,
    LayoutModule,
    SharedModule,
    AvatarModule,
    NgChatModule
  ],
  providers: [MessageService, MonitoringService, HttpExecutorService],
  exports: [
    MessageComponent,
    LoginComponent,
    DialogComponent,
    NavbarComponent,
    SidebarComponent
  ]
})
export class CoreModule {
}
