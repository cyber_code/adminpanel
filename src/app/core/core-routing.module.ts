import { SettingsComponent } from '../body/components/settings/settings.component';
import { MonitoringPageComponent } from './components/monitoring-page/monitoring-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { MainScreenComponent } from './components/main-screen/main-screen.component';
import { AuthGuard } from '../shared/auth.guards';
import { NotFoundComponent } from './components/not-found/not-found.component';

const routes: Routes = [
  { path: '', component: MainScreenComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  {
    path: 'main-screen',
    component: MainScreenComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'monitoring-page',
    component: MonitoringPageComponent
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule {}
