import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {SessionService} from 'src/app/core/services/session.service';
import {User} from '../models/user.model';
import {isNullOrUndefined} from 'util';
import {Observable} from 'rxjs';
import {ConfigurationService} from './configuration.service';


@Injectable({providedIn: 'root'})
export class AuthenticationService {
  private _isAuthenticated: boolean;
  private signinUrl: string;
  private dataUrl: string;
  private profileUrl: string;
  private userUrl: string;
  public authenticationChallange: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.isAuthenticated());
  public authenticationChallange$ = this.authenticationChallange.asObservable();

  constructor(
    private http: HttpClient,
    private session: SessionService,
    private configurationService: ConfigurationService
  ) {
    this.signinUrl = configurationService.serverSettings.identityUrl + 'connect/token';
    this.profileUrl = configurationService.serverSettings.identityUrl + 'connect/userinfo';
    this.dataUrl = `${configurationService.serverSettings.apiUrl}AAProductBuilder/`;
    this.userUrl = `${configurationService.serverSettings.identityUrl}User/`;
  }

  challange(): void {
    this.authenticationChallange.next(this.isAuthenticated());
  }

  isAuthenticated() {
    if (this._isAuthenticated) {
      return true;
    }
    const user = this.session.getUser();
    if (!isNullOrUndefined(user) && user.id !== '') {
      this._isAuthenticated = true;
    }
    return this._isAuthenticated || false;
  }

  async login(username: string, password: string) {
    const options = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };
    const user = new User();

    const req = {
      client_id: "AdminPanel",
      scope: "AdminPanel UserProfile openid profile",
      username: username,
      password: password,
      grant_type: "password",
      client_secret: "Manager123"
    };
    const params = new URLSearchParams();
    for (const key of Object.keys(req)) {
      params.set(key, req[key]);
    }

    let response = await this.http.post<any>(this.signinUrl, params.toString(), options).toPromise();
    if (response) {
      user.token = response['access_token'];
      const parsedToken = this.parseJwt(user.token);
      user.name = parsedToken['name'];
      user.email = parsedToken['email'];
      user.role = parsedToken['role'];
      user.id = parsedToken['user_id'];
      this.session.setUser(user);
      this.authenticationChallange.next(true);
      this._isAuthenticated = true;
    }
    return user;
  }

  getUerInfo(): Observable<User> {
    const loggedUser = this.session.getUser();
    const options = {
      headers: {
        Authorization: `Bearer ${loggedUser.token}`
      }
    };
    return this.http.post<User>(this.profileUrl, {}, options).pipe(
      map(response => {
        loggedUser.name = response['name'];
        loggedUser.email = response['email'];
        loggedUser.role = response['role'];
        loggedUser.username = response['prefered_username'];
        return loggedUser;
      })
    );
  }

  logout() {
    this.session.clearSession();
    this.authenticationChallange.next(false);
    this._isAuthenticated = false;
  }

  parseJwt(token) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
  }

}
