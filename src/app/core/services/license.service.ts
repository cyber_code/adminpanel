import {Injectable} from '@angular/core';
import {ConfigurationService} from './configuration.service';
import {HttpExecutorService} from './http-executor.service';
import {HttpClient} from '@angular/common/http';
import {map, catchError} from 'rxjs/operators';
import {MessageType} from '../models/message.model';
import {LicenseInfo} from '../models/license.model';

@Injectable()
export class LicenseService {

  constructor(
    private configuration: ConfigurationService,
    private httpExecutor: HttpExecutorService,
    private http: HttpClient
  ) {}

  getMachineId() {
    return this.http
      .get(this.configuration.serverSettings.identityUrl + "user/GetDeviceId", {
        responseType: "text"
      })
      .pipe(
        map((res: any) => {
          return res;
        }),
        catchError(err => {
          return this.httpExecutor.handleError(err);
        })
      );
  }

  getLicenseInfo() {
    return this.http
      .get(
        this.configuration.serverSettings.identityUrl + "user/GetLicenseInfo"
      )
      .pipe(
        map((res: LicenseInfo) => {
            return res;
        }),
        catchError(err => {
          return this.httpExecutor.handleError(err);
        })
      );
  }

  getStatus() {
    return this.http
      .get(
        this.configuration.serverSettings.identityUrl + "user/GetLicenseStatus"
      )
      .pipe(
        map((res: any) => {
          return res;
        }),
        catchError(err => {
          return this.httpExecutor.handleError(err);
        })
      );
  }

  applyLicense(params: any) {
    return this.http
      .post<any>(
        this.configuration.serverSettings.identityUrl + "user/ApplyLicense",
        params
      )
      .pipe(
        map(res => {
          return res;
        }),
        catchError(err => {
          return this.httpExecutor.handleError(err);
        })
      );
  }

  getIsSingleTenant() {
    return this.http
      .get(
        this.configuration.serverSettings.identityUrl + "user/GetIsSingleTenant"
      )
      .pipe(
        map((res: any) => {
          return res;
        }),
        catchError(err => {
          return this.httpExecutor.handleError(err);
        })
      );
  }
}
