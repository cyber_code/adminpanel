import {Helper} from 'src/app/shared/helper';
import {SubProject} from '../models/sub-project.model';
import {Injectable, Inject} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'angular-webstorage-service';
import {User} from '../models/user.model';
import {Project} from '../models/project.model';
import {DefaultSettings} from '../models/default-settings.model';
import {Catalog} from '../models/catalog.model';
import {System} from '../models/system.model';
import {WorkContext} from '../models/work-context.model';

const STORAGE_KEY = 'RAP_SESSION_KEY';

@Injectable({providedIn: 'root'})
export class SessionService {
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
  }

  setAuthorityUrl(authorityUrl: string) {
    const context = this.getWorkContext();
    context.identityUrl = authorityUrl;
    this.storage.set(STORAGE_KEY, context);
  }

  setUser(user: User) {
    const context = this.getWorkContext();
    context.user = user;
    this.storage.set(STORAGE_KEY, context);
  }

  getUser(): User {
    const context = this.getWorkContext();
    return context.user;
  }

  getToken(): string {
    const user = this.getUser();
    if (user == null) {
      return null;
    }
    return this.getUser().token;
  }

  setProject(project: Project) {
    const context = this.getWorkContext();
    context.project = project;
    this.storage.set(STORAGE_KEY, context);
  }

  setSubProject(subProject: SubProject) {
    const context = this.getWorkContext();
    context.subProject = subProject;
    this.storage.set(STORAGE_KEY, context);
  }

  getProject(): Project {
    const context = this.getWorkContext();
    return context.project;
  }

  getSubProject(): SubProject {
    const context = this.getWorkContext();
    return context.subProject;
  }

  clearSession() {
    this.storage.remove(STORAGE_KEY);
  }

  getWorkContext(): WorkContext {
    let context = this.storage.get(STORAGE_KEY) as WorkContext;
    if (context == null) {
      context = new WorkContext();

      context.defaultSettings = new DefaultSettings(new Catalog(), new System());
      this.storage.set(STORAGE_KEY, context);
    }
    return context;
  }

  getDefaultSettings(): DefaultSettings {
    const context = this.getWorkContext();
    return context.defaultSettings;
  }

  setDefaultSettings(defaultsettings: DefaultSettings) {
    const context = this.getWorkContext();
    context.defaultSettings = defaultsettings;
    this.storage.set(STORAGE_KEY, context);
  }

  getHash() {
    const context = this.getWorkContext();
    return Helper.createHash(
      context.user.tenantId,
      context.project.id,
      context.subProject.id,
      context.defaultSettings.system.id,
      context.defaultSettings.catalog.id
    );
  }
}
