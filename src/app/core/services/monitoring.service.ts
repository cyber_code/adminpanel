import { Injectable } from '@angular/core';
import { ConfigurationService } from './configuration.service';
import { HttpExecutorService } from './http-executor.service';
import { Observable } from 'rxjs';
import { ServiceInfo, CommandLog, QueryLog } from '../models/service-Info.model';
import {GetStatus} from '../models/command-queries/command-queries';

@Injectable()
export class MonitoringService {
  private apiUrl: string;
  constructor(private configuration: ConfigurationService, private httpExecutor: HttpExecutorService) {
   this.apiUrl = `${configuration.serverSettings.apiUrl}Services/Ping?command=%7B%7D/`;
  }
  getStatus(): Observable<ServiceInfo[]> {
    return this.httpExecutor.executeQuery<ServiceInfo[]>(this.apiUrl, new GetStatus());
  }
  getCommandLog():  Observable<CommandLog[]> {
    return this.httpExecutor.executeQuery<CommandLog[]>(this.apiUrl, new GetStatus());
  }
  getQueryLog(): Observable<QueryLog[]> {
    return this.httpExecutor.executeQuery<QueryLog[]>(this.apiUrl, new GetStatus());
  }
}
