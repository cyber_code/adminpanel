import {Component, OnInit} from '@angular/core';
import { LicenseService } from '../../services/license.service';

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  public userProfile: any;
  public isSingleTenant = false;

  constructor(private licenceService: LicenseService) {}

  async ngOnInit() {
    const result = await this.getIsSingleTenant();
    if ('isSingleTenant' in result) {
      this.isSingleTenant = result.isSingleTenant;
    }
    this.userProfile = JSON.parse(localStorage.getItem("RAP_SESSION_KEY")).user;
  }

  async getIsSingleTenant() {
    return this.licenceService.getIsSingleTenant().toPromise();
  }
}
