import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AuthenticationService} from '../../services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MessageService} from '../../services/message.service';
import {LicenseInfo, LicenseStatus} from '../../models/license.model';
import {LicenseService} from '../../services/license.service';


@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public licenseForm: FormGroup;

  public licenseInfo: LicenseInfo;

  public progress = false;
  public loading = false;
  public submitted = false;

  public message: string;
  public currentYear: number;
  public machineId = "";

  public newLicenceApplied = false;
  public showLogin = true;
  public showAddLicence = false;
  public showAddLicenceBackBtn = false;
  public showLicenceDetails = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private messageService: MessageService,
    private licenceService: LicenseService
  ) {}

  async ngOnInit() {
    this.initForms();
    let licenceStatus = await this.getLicenceStatus();
    
    this.currentYear = new Date().getFullYear();
    
    if (licenceStatus.status) {
      this.showLoginForm();

      if (this.authenticationService.isAuthenticated()) {
        this.router.navigateByUrl("/body");
      }
    } else {
      this.showApplyNewLicenceForm();
    }
  }

  get l() {
    return this.licenseForm.controls;
  }

  get f() {
    return this.loginForm.controls;
  }

  get g() {
    return this.licenseForm.controls;
  }

  getLicenceStatus() {
    return this.licenceService.getStatus().toPromise();
  }

  getMachineId() {
    return this.licenceService.getMachineId().toPromise();
  }

  getLicenceInfo() {
    this.licenceService.getLicenseInfo().subscribe(
      data => {
        this.licenseInfo = data;
      },
      error => {
        this.messageService.sendMessage({
          message: error.error_description,
          title: error.error,
          type: "danger"
        });
      }
    );
  }

  applyLicence(licenceKey: any) {
    return this.licenceService.applyLicense(licenceKey).toPromise();
  }

  initForms() {
    this.loginForm = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required]
    });

    this.licenseForm = this.formBuilder.group({
      licenseKey: ["", Validators.required]
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      if (this.f.username.value.trim("") === "") {
        this.messageService.sendMessage({
          message: "Username must not be empty!",
          title: "Error",
          type: "danger"
        });
      } else if (this.f.password.value.trim("") === "") {
        this.messageService.sendMessage({
          message: "Password must not be empty!",
          title: "Error",
          type: "danger"
        });
      }
      return;
    }

    this.loading = true;
    this.authenticationService
      .login(this.f.username.value, this.f.password.value)
      .then(
        data => {
          this.router.navigateByUrl("/body");
        },
        err => {
          this.messageService.sendMessage({
            message: err.error.error_description,
            title: err.error.error,
            type: "danger"
          });
          this.loading = false;
        }
      );
  }

  async applyLicenceSubmit() {
    this.progress = true;
    const licenseSubmitParam = { Key: this.l.licenseKey.value };
    this.newLicenceApplied = await this.applyLicence(licenseSubmitParam);
    if (this.newLicenceApplied) {
      setTimeout(() => {
        this.showLicenceDetailsForm();
        this.progress = false;
      }, 3000);
    } else {
      this.messageService.sendMessage({
        message: "Something went wrong during licence application!",
        title: "Oops!",
        type: "danger"
      });
    }
  }

  showLoginForm() {
    this.showLogin = true;
    this.showAddLicence = false;
    this.showLicenceDetails = false;
  }

  async showLicenceDetailsForm() {
    const licenceStatus = await this.getLicenceStatus();
    if (licenceStatus.status) {
      this.getLicenceInfo();
      this.showLogin = false;
      this.showAddLicence = false;
      this.showLicenceDetails = true;
    } else {
      this.messageService.sendMessage({
        message: licenceStatus.message,
        title: "Oops!",
        type: "danger"
      });
    }
  }

  async showApplyNewLicenceForm() {
    const licenceStatus = await this.getLicenceStatus();
    if (licenceStatus.status) {
      this.showAddLicenceBackBtn = true;
    }
    this.machineId = await this.getMachineId();
    this.showLogin = false;
    this.showAddLicence = true;
    this.showLicenceDetails = false;
  }
}
