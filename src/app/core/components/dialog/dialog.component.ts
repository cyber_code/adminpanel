import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  public opened = true;

  constructor() {
  }

  ngOnInit() {
  }

  public close(status) {
    this.opened = false;
  }

  public open() {
    this.opened = true;
  }
}
