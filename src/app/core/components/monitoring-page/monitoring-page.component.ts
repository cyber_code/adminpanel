import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {MonitoringService} from '../../services/monitoring.service';
import {CommandLog, QueryLog, ServiceInfo} from '../../models/service-Info.model';

@Component({
  selector: 'app-monitoring-page',
  templateUrl: './monitoring-page.component.html',
  styleUrls: ['./monitoring-page.component.css']
})
export class MonitoringPageComponent implements OnInit {
  public tableData: ServiceInfo[];
  public commandLog: CommandLog[];
  public queryLog: QueryLog[];
  public lastUpdate: any;
  public today: number = Date.now();

  get now(): string {
    return Date();
  }

  public title: any;
  public data: any;

  constructor(private monitoringService: MonitoringService, cd: ChangeDetectorRef) {
  }

  getStatus() {
    this.monitoringService.getStatus().subscribe(data => {
        this.tableData = (data as any).ServiceInfo;
      },
      (err) => {
      });
  }

  getCommandLog() {
    this.monitoringService.getCommandLog().subscribe(data => {
        this.commandLog = (data as any).CommandLog;
      },
      (err) => {
      });
  }

  getQueryLog() {
    this.monitoringService.getQueryLog().subscribe(data => {
        this.queryLog = (data as any).QueryLog;
      },
      (err) => {
      });
  }

  ngOnInit() {
    this.getStatus();
    this.getCommandLog();
    this.getQueryLog();
  }

  refreshData() {
    this.getStatus();
    this.now;
  }
}
