import {Component, OnInit} from '@angular/core';
import {MessageService} from '../../services/message.service';

@Component({
  selector: 'app-mess',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  message: string;
  messageType: string;
  messageTitle: string;
  showMessage: boolean;
  icon: string;

  constructor(private messageService: MessageService) {
  }

  ngOnInit() {
    this.messageService.getMessage().subscribe(response => {
      this.message = response.message;
      this.messageTitle = response.title;
      this.messageType = response.type;
      this.showMessage = true;
      this.icon = this.returnIcon(this.messageType);
      setTimeout(() => {
        this.showMessage = false;
      }, 6000);
    });

  }

  returnIcon(messageType: string): string {
    switch (messageType) {
      case 'danger':
        return 'ban';
      case 'success':
        return 'check';
      case 'warning':
        return 'warning';
    }
  }

}






