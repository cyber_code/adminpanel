import * as MD5 from './md5';

export class Helper {
  public static getValueFromQueryString(url: string, key: string) {
    key = key.replace(/[\[\]]/g, '\\$&');
    const regex = new RegExp('[?&]' + key + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) {
      return null;
    }
    if (!results[2]) {
      return '';
    }
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  public static createHash(tenantId: string, projectId: string, subProjectId: string, systemId: string, catalogId: string) {
    const str = tenantId + '' + projectId + '|' + subProjectId + '|' + 'B5BCC43F-AFC8-4F67-8EB1-B633994CE363' + '|' + catalogId;
    return MD5(str);
  }

  public static dataSourceFilter(dsFilter: string, searchText: string): boolean {
    return dsFilter.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
  }
}
