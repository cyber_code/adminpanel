import {BrowserModule} from '@angular/platform-browser';
import {NgModule, APP_INITIALIZER} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DialogsModule, DialogModule} from '@progress/kendo-angular-dialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {HttpClientModule, HttpClient } from '@angular/common/http';
import {LOCAL_STORAGE, StorageServiceModule} from 'angular-webstorage-service';
import {ConfigurationService} from './core/services/configuration.service';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import {LicenseService} from './core/services/license.service';
import {LicenseStatus} from './core/models/license.model';
import {NotificationModule} from '@progress/kendo-angular-notification';



const appInitializerFn = (appConfig: ConfigurationService) => {
  return () => {
    return appConfig.load();
  };
};


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    StorageServiceModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DialogsModule,
    BrowserAnimationsModule,
    DialogModule,
    ButtonsModule,
    NotificationModule
  ],
  providers: [
    ConfigurationService, LicenseService, LicenseStatus,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFn,
      multi: true,
      deps: [ConfigurationService]
    },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
