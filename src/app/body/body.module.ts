import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NewUserComponent} from './components/new-user/new-user.component';
import {DialogModule} from '@progress/kendo-angular-dialog';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {BodyComponent} from './body.component';
import {UserRightsComponent} from './components/user-rights/user-rights.component';
import {GridModule, SharedModule} from '@progress/kendo-angular-grid';
import {BodyRoutingModule} from './body-routing.module';
import {UserRolesComponent} from './components/user-roles/user-roles.component';
import {TenantDetailsComponent} from './components/tenant-details/tenant-details.component';
import {NewTenantComponent} from './components/new-tenant/new-tenant.component';
import {UserListComponent} from './components/user-list/user-list.component';
import {CoreModule} from '../core/core.module';
import {CoreRoutingModule} from '../core/core-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TenantListComponent} from './components/tenant-list/tenant-list.component';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';
import {UserService} from './services/User.service';
import {TenantService} from './services/Tenant.service';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {EditUserService} from './services/EditUser.service';
import {UserRoleService} from './services/UserRole.Service';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {MainBodyComponent} from './components/main-body/main-body.component';
import {RolesComponent} from './components/roles/roles.component';
import {RolesService} from './services/Roles.Service';
import {NewAdminComponent} from './components/new-admin/new-admin.component';
import {AdminPanelUserService} from './services/AdminPanelUser.service';
import { AdminListComponent } from './components/admin-list/admin-list.component';
import { LayoutModule } from '@progress/kendo-angular-layout';

@NgModule({
  declarations: [
    BodyComponent,
    NewUserComponent,
    UserRightsComponent,
    UserRolesComponent,
    TenantDetailsComponent,
    NewTenantComponent,
    UserListComponent,
    TenantListComponent,
    MainBodyComponent,
    RolesComponent,
    NewAdminComponent,
    AdminListComponent
  ],
  imports: [
    CommonModule,
    DialogModule,
    BodyRoutingModule,
    ButtonsModule,
    DialogModule,
    ButtonsModule,
    GridModule,
    CoreModule,
    CoreRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    InfiniteScrollModule,
    HttpClientJsonpModule,
    DropDownsModule,
    AngularMultiSelectModule,
    SharedModule,
    LayoutModule
  ],
  providers: [UserService, TenantService, EditUserService, UserRoleService, RolesService,AdminPanelUserService]
})
export class BodyModule {
}
