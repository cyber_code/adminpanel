import {NewUserComponent} from './components/new-user/new-user.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BodyComponent} from './body.component';
import {TenantDetailsComponent} from './components/tenant-details/tenant-details.component';
import {UserListComponent} from './components/user-list/user-list.component';
import {NewTenantComponent} from './components/new-tenant/new-tenant.component';
import {TenantListComponent} from './components/tenant-list/tenant-list.component';
import {AuthGuard} from '../shared/auth.guards';
import {MainBodyComponent} from './components/main-body/main-body.component';
import {RolesComponent} from './components/roles/roles.component';
import {NewAdminComponent} from './components/new-admin/new-admin.component';
import {AdminListComponent} from './components/admin-list/admin-list.component';
import {SettingsComponent} from './components/settings/settings.component';

const routes: Routes = [
  {
    path: '',
    component: BodyComponent,
    canActivate: [AuthGuard],
    children: [
      {path: '', component: MainBodyComponent, canActivate: [AuthGuard]},
      {
        path: 'new-user',
        component: NewUserComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'tenant-details/:id',
        component: TenantDetailsComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'user-list',
        component: UserListComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'new-tenant',
        component: NewTenantComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'tenant-list',
        component: TenantListComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'roles',
        component: RolesComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'new-admin',
        component: NewAdminComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'admin-list',
        component: AdminListComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'settings',
        component: SettingsComponent,
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BodyRoutingModule {
}
