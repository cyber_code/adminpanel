import {Component, OnInit} from '@angular/core';

import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {UserService} from '../../services/User.service';
import {Role} from '../../models/role';
import {roles} from './roles';
import {UserRoleService} from '../../services/UserRole.Service';


@Component({
  selector: 'app-user-roles',
  templateUrl: './user-roles.component.html',
  styleUrls: ['./user-roles.component.css']
})
export class UserRolesComponent implements OnInit {

  public itemToRemove: any;
  resetOpened: boolean = false;
  removeOpened: boolean = false;
  public roles: any[] = roles;
  public formGroup: FormGroup;
  private editedRowIndex: number;
  public removeConfirmationSubject: Subject<boolean> = new Subject<boolean>();

  constructor(private formBuilder: FormBuilder, private userRoleService: UserRoleService) {
    this.createFormGroup = this.createFormGroup.bind(this);
    this.removeConfirmation = this.removeConfirmation.bind(this);
  }

  ngOnInit() {
  }

  public createFormGroup(args: any): FormGroup {
    const item = args.isNew ? new Role() : args.dataItem;

    this.formGroup = this.formBuilder.group({
      'ID': [item.ID, Validators.required],
      'name': [item.name, Validators.required],
      'processIntelligence': [item.processIntelligence, Validators.required],
      'testDataIntelligence': [item.testDataIntelligence, Validators.required],
      'testCaseModule': [item.testCaseModule, Validators.required],
      'insightsModule': [item.insightsModule, Validators.required],
      'productModule': [item.productModule, Validators.required]
    });

    return this.formGroup;
  }

  public confirmRemove(shouldRemove: boolean): void {
    this.removeConfirmationSubject.next(shouldRemove);
    this.itemToRemove = null;
  }

  public removeConfirmation(dataItem): Subject<boolean> {
    this.itemToRemove = dataItem;
    return this.removeConfirmationSubject;
  }

  public addHandler({sender}) {
    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      'ID': new FormControl(),
      'name': new FormControl('', Validators.required),
      'processIntelligence': new FormControl(false),
      'testDataIntelligence': new FormControl(false),
      'testCaseModule': new FormControl(false),
      'insightsModule': new FormControl(false),
      'productModule': new FormControl(false)
    });

    sender.addRow(this.formGroup);
  }

  public editHandler({sender, rowIndex, dataItem}) {
    this.formGroup = new FormGroup({
      'ID': new FormControl(dataItem.ID, Validators.required),
      'name': new FormControl(dataItem.name, Validators.required),
      'processIntelligence': new FormControl(dataItem.processIntelligence, Validators.required),
      'testDataIntelligence': new FormControl(dataItem.testDataIntelligence, Validators.required),
      'testCaseModule': new FormControl(dataItem.testCaseModule, Validators.required),
      'insightsModule': new FormControl(dataItem.insightsModule, Validators.required),
      'productModule': new FormControl(dataItem.productModule, Validators.required)
    });

    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({sender, rowIndex}) {
    this.closeEditor(sender, rowIndex);
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  protected saveHandler({sender, rowIndex, formGroup, isNew}) {
    const role: Role = formGroup.value;
    sender.closeRow(rowIndex);
  }

}
