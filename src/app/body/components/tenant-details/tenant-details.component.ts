import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { UserService } from "../../services/User.service";
import { tenants } from "../tenant-list/tenants";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MessageService } from "../../../core/services/message.service";
import { TenantService } from "../../services/Tenant.service";
import { DomSanitizer } from "@angular/platform-browser";
import { UserRoleService } from "../../services/UserRole.Service";

@Component({
  selector: "app-tenant-details",
  templateUrl: "./tenant-details.component.html",
  styleUrls: ["./tenant-details.component.css"]
})
export class TenantDetailsComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private tenantService: TenantService,
    private userRoleService: UserRoleService,
    private _sanitizer: DomSanitizer,
    private router: Router
  ) {}

  tenantData: any;
  tenantId: any;
  editModeBoolean: boolean = false;
  tenants: any[] = tenants;
  tenantDetailsForm: FormGroup;
  submitted: boolean = false;
  addUserPopup: boolean = false;
  deletePopupOpened = false;
  deleteTenantId: any;
  tenantUsers: any[] = [];
  newUser: FormGroup;
  loading = false;
  tenantsList: any[] = [];
  selectedItems: any[] = [];
  dropdownList: any[] = [];
  dropdownSettings = {};
  settings = {};
  addUserButton: boolean = true;
  licenses: any[] = [];
  tenantImage: any = "";

  ngOnInit() {
    this.tenantId = this.route.snapshot.paramMap.get("id");
    this.getAllUsers();
    this.userRoleService.getAllRoles().subscribe(data => {
      this.dropdownList = data;
      this.dropdownList.forEach(item => {
        item.itemName = item.name;
      });
    });
    this.tenantService.getAllLiceses().subscribe(data => {
      this.tenantService
        .getAllLicesesAssigneedToTenant(this.tenantId)
        .subscribe(res => {
          res = res ? res : [];
          this.licenses = data.map(license => {
            if (res.find(el => license.id === el.id)) {
              return { ...license, checked: true };
            }
            return { ...license, checked: false };
          });
        });
      });
    

    this.tenantService.getTenantById(this.tenantId).subscribe(res => {
      this.tenantData = res;
      if (this.tenantData.image) {
        this.tenantImage = atob(this.tenantData.image);
      }
      this.tenantDetailsForm = this.formBuilder.group({
        id: ["", Validators.required],
        name: [{ value: "", disabled: true }, Validators.required],
        email: [{ value: "", disabled: true }, Validators.required],
        image: [""],
        description: [{ value: "", disabled: true }],
        address: [""],
        address2: [""],
        city: [""],
        state: [""],
        zip: [""]
      });
      this.tenantDetailsForm.patchValue({ ...this.tenantData });
    });
    this.newUserMethod();
  }

  editTenant(id) {
    this.f["name"].enable();
    this.f["email"].enable();
    this.f["description"].enable();
    this.editModeBoolean = true;
  }

  deleteTenant(tenantId) {
    this.deletePopupOpened = true;
    this.deleteTenantId = tenantId;
  }

  confirmDelete(tenantId) {
    this.deletePopupOpened = false;
    this.tenantService.deleteTenant(this.tenantId).subscribe(
      data => {
        this.router.navigateByUrl("/body");
        this.messageService.sendMessage({
          message: "Update Successful!",
          title: "Update Successful!",
          type: "info"
        });
      },
      error => {
        this.messageService.sendMessage({
          message: error.error.error,
          title: "Registration Error",
          type: "danger"
        });
      }
    );
  }

  discardChanges(id) {
    this.f["name"].disable();
    this.f["email"].disable();
    this.f["description"].disable();
    this.editModeBoolean = false;
    this.tenantDetailsForm.patchValue({
      ...this.tenantData
    });
  }

  addUser() {
    this.addUserPopup = true;
  }

  getCheckedLicenseIds() {
    return this.licenses.filter(license => license.checked).map(lc => lc.id);
  }

  onEditSubmit() {
    this.submitted = true;
    this.editModeBoolean = false;
    this.f["name"].disable();
    this.f["email"].disable();
    this.f["description"].disable();

    this.tenantService
      .updateTenant(
        this.f.id.value,
        this.f.name.value,
        this.f.email.value,
        this.f.image.value,
        this.getCheckedLicenseIds(),
        this.f.address.value,
        this.f.address2.value,
        this.f.city.value,
        this.f.state.value,
        this.f.zip.value,
        this.f.description.value
      )
      .subscribe(
        data => {
          this.messageService.sendMessage({
            message: "Update Successful!",
            title: "Update",
            type: "info"
          });
          this.tenantService.getTenantById(this.tenantId).subscribe(res => {
            this.tenantData = res;
          });
        },
        error => {
          this.messageService.sendMessage({
            message: error.error.error,
            title: "Registration Error",
            type: "danger"
          });
          this.loading = false;
        }
      );
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true };
  }

  get f() {
    return this.tenantDetailsForm.controls;
  }

  get g() {
    return this.newUser.controls;
  }

  closePopup() {
    this.newUser.reset();
    this.addUserPopup = false;
    this.newUserMethod();
  }

  onAddSubmit() {
    this.addUserButton = false;
    this.newUser.disable;
    this.userService
      .addUser(
        this.g.fullName.value,
        this.g.userName.value,
        this.g.email.value,
        this.g.phone.value,
        this.g.password.value,
        this.g.confirmPassword.value,
        this.g.roles.value,
        this.g.tenantId.value
      )
      .subscribe(
        data => {
          this.addUserButton = true;
          this.newUser.enable;
          this.newUser.reset();
          this.newUserMethod();
          this.addUserPopup = false;
          this.getAllUsers();
        },
        error => {
          this.addUserButton = true;
          this.messageService.sendMessage({
            message: error.error.error,
            title: "Registration Error",
            type: "danger"
          });
        }
      );
  }

  returnLicenseClass(license) {
    if (license.parentId === 0) {
      return "level-1-license";
    } else {
      const parent = this.licenses.find(x => x.id === license.parentId);
      if (parent.parentId === 0) {
        return "level-2-license";
      } else {
        return "level-3-license";
      }
    }
  }

  onLicenseChange(event, licenseToChange) {
    const firstChildren = this.licenses.filter(
      license => license.parentId === licenseToChange.id
    );
    const secondChildren = this.licenses.filter(license =>
      firstChildren.find(el => el.id === license.parentId)
    );

    let licenses = this.licenses.map(license => {
      if (
        licenseToChange.id === license.id ||
        firstChildren.find(el => el.id === license.id) ||
        secondChildren.find(el => el.id === license.id)
      ) {
        return { ...license, checked: event.target.checked };
      }
      return license;
    });
    this.checkForParents(licenses);
  }

  checkForParents(licenses) {
    this.licenses = licenses.map(license => {
      const firstChildren = licenses.filter(el => el.parentId === license.id);
      const secondChildren = licenses.filter(licenseEl =>
        firstChildren.find(el => el.id === licenseEl.parentId)
      );
      if (secondChildren.length > 0) {
        if (
          secondChildren.filter(el => el.checked).length ===
          secondChildren.length
        ) {
          return { ...license, checked: true };
        } else {
          return { ...license, checked: false };
        }
      } else if (firstChildren.length > 0) {
        if (
          firstChildren.filter(el => el.checked).length === firstChildren.length
        ) {
          return { ...license, checked: true };
        } else {
          return { ...license, checked: false };
        }
      }
      return license;
    });
  }

  getAllUsers() {
    this.userService.getUserByTenantId(this.tenantId).subscribe(res => {
      if (res) {
        this.tenantUsers = res.map(user => {
          let roleNames = "";
          user.userRoles.map(roleObj => {
            roleNames +=
              roleNames === "" ? roleObj.role.name : ", " + roleObj.role.name;
          });
          return { ...user, roleNames: roleNames };
        });
      }
    });
  }

  newUserMethod() {
    this.newUser = this.formBuilder.group(
      {
        fullName: ["", Validators.required],
        userName: ["", Validators.required],
        email: ["", [Validators.required, Validators.email]],
        phone: ["", Validators.required],
        roles: [[], Validators.required],
        password: [
          "",
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(10)
          ]
        ],
        confirmPassword: ["", Validators.required],
        tenantId: [this.tenantId, Validators.required]
      },
      { validator: this.checkPasswords }
    );
  }
}
