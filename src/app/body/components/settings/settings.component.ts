import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MessageService} from '../../../core/services/message.service';
import {AdminPanelUserService} from '../../services/AdminPanelUser.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  changePassword: FormGroup;
  loading = false;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private adminPanelUserService: AdminPanelUserService) {}

  ngOnInit() {
    this.changePassword = this.formBuilder.group(
      {
        oldPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(10)]],
        newPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(10)]],
        confirmPassword: ['', Validators.required]
      },
      {validator: this.checkPasswords}
    );

  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.newPassword.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : {notSame: true};
  }

  get f() {
    return this.changePassword.controls;
  }

  onSubmit() {
    this.loading = true;
    this.adminPanelUserService.changePassword(
      this.f.oldPassword.value,
      this.f.newPassword.value,
      this.f.confirmPassword.value
    ).subscribe(
      data => {
        this.messageService.sendMessage({
          message: data['message'],
          title: 'Success!',
          type: 'info'
        });
        this.changePassword.reset();
        this.loading = false;
      },
      err => {
        this.messageService.sendMessage({
          message: err.error.error,
          title: 'Registration Error',
          type: 'danger'
        });
        this.loading = false;
      });
  }
}
