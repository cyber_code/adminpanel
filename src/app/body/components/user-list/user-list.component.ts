import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {User} from '../../models/user';
import {UserService} from '../../services/User.service';
import {Subject} from 'rxjs/Subject';
import {MessageService} from '../../../core/services/message.service';
import {UserRoleService} from '../../services/UserRole.Service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  selectedUserId: any;
  public itemToRemove: any;
  resetOpened: boolean = false;
  public users: any[] = [];
  public formGroup: FormGroup;
  private editedRowIndex: number;
  public removeConfirmationSubject: Subject<boolean> = new Subject<boolean>();
  public allRoles: any[] = [];
  rolesEditMode:boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private messageService: MessageService,
    private userRoleService: UserRoleService
  ) {
    this.createFormGroup = this.createFormGroup.bind(this);
    this.removeConfirmation = this.removeConfirmation.bind(this);
  }

  ngOnInit() {
    this.userRoleService.getAllRoles().subscribe(res => {
      this.allRoles = res;
      this.allRoles = this.allRoles.map(role => {
        return {
          id: role.id,
          name: role.name
        };
      });
    });
    this.userService.getAllUsers().subscribe((res: any) => {
      this.users = res.map(user => {
        return {...user, userRoles: user.userRoles ? user.userRoles.map(userRole => ({id: userRole.role.id, name: userRole.role.name})) : []};
      });
    });
  }


  public createFormGroup(args: any): FormGroup {
    let item = args.isNew ? new User() : args.dataItem;
    this.formGroup = this.formBuilder.group({
      id: [item.id, Validators.required],
      fullName: [item.fullName, Validators.required],
      userName: [item.userName, Validators.required],
      phone: [item.phone],
      email: [item.email],
      userRoles: [item.userRoles, Validators.required],
      tenantId: [item.tenantId, Validators.required]
    });
    return this.formGroup;
  }

  resetPassword(userId) {
    this.selectedUserId = userId;
    this.resetOpened = true;
  }

  confirmReset(userId) {
    this.resetOpened = false;
    this.userService.resetPassword(userId).subscribe(res => {
      this.messageService.sendMessage({
        message: 'Password Reset Successfully!',
        title: 'Action',
        type: 'info'
      });
    });
  }

  public confirmRemove(shouldRemove: boolean): void {
    this.userService.deleteUser(this.itemToRemove.id).subscribe(res => {
      this.messageService.sendMessage({
        message: 'User Deleted!',
        title: 'Action',
        type: 'info'
      });
    });
    this.removeConfirmationSubject.next(shouldRemove);
    this.itemToRemove = null;
  }

  public removeConfirmation(dataItem): Subject<boolean> {
    this.itemToRemove = dataItem;
    return this.removeConfirmationSubject;
  }

  public editHandler({sender, rowIndex, dataItem}) {
    this.formGroup = new FormGroup({
      id: new FormControl(dataItem.id, Validators.required),
      fullName: new FormControl(dataItem.fullName, Validators.required),
      userName: new FormControl(dataItem.userName, Validators.required),
      phone: new FormControl(dataItem.phone),
      email: new FormControl(dataItem.email),
      userRoles: new FormControl(dataItem.userRoles, Validators.required),
      tenantId: new FormControl(dataItem.tenantId, Validators.required)
    });
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({sender, rowIndex}) {
    this.closeEditor(sender, rowIndex);
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  protected saveHandler({sender, rowIndex, formGroup, isNew}) {
    let formValues = formGroup.value;
    if(formValues.userRoles.length===0){
      this.messageService.sendMessage({
        message: 'Roles can not be empty!',
        title: 'Action',
        type: 'danger'
      });
    }
    else {
      this.userService
      .updateUser(
        formValues.id,
        formValues.userName,
        formValues.email,
        formValues.phone,
        formValues.fullName,
        formValues.tenantId,
        formValues.userRoles
      )
      .subscribe(res => {
        const user: User = formGroup.value;
        sender.closeRow(rowIndex);
        this.messageService.sendMessage({
          message: 'User Updated!',
          title: 'Action',
          type: 'info'
        });
      });
    }
  }

  public roles(id: number): any {
    return this.allRoles.find(x => x.id === id);
  }

  public category(role): any {
    return this.allRoles.find(x => x.id === role.id);
  }


}
