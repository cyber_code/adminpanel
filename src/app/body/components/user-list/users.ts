export const users = [{
    'ID': 1,
    'username': 'Chai',
    'role': 'admin',
    'tenantId': 1
  }, {
    'ID': 2,
    'username': 'Chang',
    'role': 'admin',
    'tenantId': 3
  }, {
    'ID': 3,
    'username': 'Syrup',
    'role': 'manager',
    'tenantId': 4
  }, {
    'ID': 4,
    'username': 'Chef Anton',
    'role': 'Visitor',
    'tenantId': 2
  }, {
    'ID': 5,
    'username': 'Chai',
    'role': 'qa',
    'tenantId': 1
  }, {
    'ID': 6,
    'username': 'Chang',
    'role': 'admin',
    'tenantId': 3
  }, {
    'ID': 7,
    'username': 'Aniseed Syrup',
    'role': 'Client',
    'tenantId': 4
  }, {
    'ID': 8,
    'username': 'Chef Anton',
    'role': 'Editor',
    'tenantId': 2
  }, {
    'ID': 9,
    'username': 'Chai',
    'role': 'Super Admin',
    'tenantId': 1
  }, {
    'ID': 10,
    'username': 'Chang',
    'role': 'Admin',
    'tenantId': 3
  }, {
    'ID': 11,
    'username': 'Chang2',
    'role': 'Moderator',
    'tenantId': 4
  }, {
    'ID': 12,
    'username': 'Chef Anton',
    'role': 'Editor',
    'tenantId': 2
  }
];
