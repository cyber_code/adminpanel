import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  OnInit,
  Output
} from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";
import { Router } from "@angular/router";
import { MessageService } from "../../../core/services/message.service";
import { TenantService } from "../../services/Tenant.service";
import { LicenseService } from "src/app/core/services/license.service";
import { UserRoleService } from "../../services/UserRole.Service";

@Component({
  selector: "app-new-tenant",
  templateUrl: "./new-tenant.component.html",
  styleUrls: ["./new-tenant.component.css"]
})
export class NewTenantComponent implements OnInit {
  private file: any = "";
  private base64textString: String = "";
  tenantId: String = "";
  newTenant: FormGroup;
  loading = false;
  submitted = false;
  @Output()
  uploadEvent: EventEmitter<any> = new EventEmitter<any>();
  licenses: any[] = [];
  public isSingleTenant = false;
  public roles = [];

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private tenantService: TenantService,
    private host: ElementRef<HTMLInputElement>,
    private licenseService: LicenseService,
    private userRoleService: UserRoleService
  ) {}

  async ngOnInit() {
    this.newTenant = this.formBuilder.group(
      {
        name: ["", Validators.required],
        userName: ["", Validators.required],
        email: ["", [Validators.required, Validators.email]],
        image: [""],
        password: [
          "",
          [
            Validators.required,
            Validators.minLength(6),
            Validators.maxLength(10)
          ]
        ],
        confirmPassword: ["", Validators.required],
        description: [""],
        address: [""],
        address2: [""],
        city: [""],
        state: [""],
        zip: [""],
        roleId: ["", Validators.required]
      },
      { validator: this.checkPasswords }
    );
    const result = await this.getIsSingleTenant();
    if ("isSingleTenant" in result && result.isSingleTenant) {
      this.isSingleTenant = result.isSingleTenant;
      this.router.navigateByUrl("/new-user");
      return;
    }

    this.tenantService.getAllLiceses().subscribe(data => {
      this.licenses = data;
    });

    this.userRoleService.getAllRoles().subscribe(data => {
      this.roles = data;
    });
  }

  returnLicenseClass(license) {
    if (license.parentId === 0) {
      return "";
    } else {
      const parent = this.licenses.find(x => x.id === license.parentId);
      if (parent.parentId === 0) {
        return "level-2-license";
      } else {
        return "level-3-license";
      }
    }
  }

  async getIsSingleTenant() {
    return this.licenseService.getIsSingleTenant().toPromise();
  }

  checkPasswords(group: FormGroup) {
    // here we have the 'passwords' group
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : { notSame: true };
  }

  get f() {
    return this.newTenant.controls;
  }

  _handleReaderLoaded(readerEvt) {
    let binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    this.newTenant.patchValue({
      image: this.base64textString
    });
  }

  public dropHandler(event: any) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();
      reader.onloadend = ev => {
        this.uploadEvent.emit({ url: ev, file: event.target.files[0] });
      };
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = this._handleReaderLoaded.bind(this);
    }
    event.preventDefault();
  }

  onLicenseChange(event, licenseToChange) {
    const firstChildren = this.licenses.filter(license => license.parentId === licenseToChange.id);
    const secondChildren = this.licenses.filter(license => firstChildren.find(el => el.id === license.parentId));

    let licenses = this.licenses.map(license => {
      if (licenseToChange.id === license.id ||
        firstChildren.find(el => el.id === license.id) ||
        secondChildren.find(el => el.id === license.id)) {
        return { ...license, checked: event.target.checked };
      } 
      return license;
    });
    this.checkForParents(licenses);
  }

  checkForParents(licenses) {
    this.licenses = licenses.map(license => {
      const firstChildren = licenses.filter(el => el.parentId === license.id);
      const secondChildren = licenses.filter(licenseEl => firstChildren.find(el => el.id === licenseEl.parentId));
      if (secondChildren.length > 0) {
        if (secondChildren.filter(el => el.checked).length === secondChildren.length) {
          return {...license, checked: true};
        } else {
          return {...license, checked: false};
        }
      } else if (firstChildren.length > 0) {
        if (firstChildren.filter(el => el.checked).length === firstChildren.length) {
          return {...license, checked: true};
        } else {
          return {...license, checked: false}
        }
      } 
      return license;
    });
  }

  getCheckedLicenseIds() {
    return this.licenses.filter(license => license.checked).map(lc => lc.id);
  }

  onSubmit() {
    this.loading = true;
    this.tenantService
      .addTenant(
        this.f.name.value,
        this.f.userName.value,
        this.f.email.value,
        this.f.image.value,
        this.f.password.value,
        this.f.confirmPassword.value,
        this.getCheckedLicenseIds(),
        this.f.address.value,
        this.f.address2.value,
        this.f.city.value,
        this.f.state.value,
        this.f.zip.value,
        this.f.description.value,
        this.f.roleId.value
      )
      .subscribe(
        data => {
          this.router.navigateByUrl("/tenant-list");
        },
        err => {
          this.messageService.sendMessage({
            message: err.error.error,
            title: "Registration Error",
            type: "danger"
          });
          this.loading = false;
        }
      );
  }
}
