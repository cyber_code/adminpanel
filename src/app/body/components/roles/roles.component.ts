import {Component, OnInit} from '@angular/core';

import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {Role} from '../../models/role';
import {RolesService} from '../../services/Roles.Service';
import {MessageService} from '../../../core/services/message.service';
import {RowArgs} from '@progress/kendo-angular-grid';
import {State, process} from '@progress/kendo-data-query';
import { LicenseService } from 'src/app/core/services/license.service';
import { SessionService } from 'src/app/core/services/session.service';
import { TenantService } from '../../services/Tenant.service';
import { UserService } from '../../services/User.service';
import { Guid } from 'src/app/shared/guid';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  public itemToRemove: any;
  resetOpened: boolean = false;
  removeOpened: boolean = false;
  public roles: any[] = [];
  public userPermissionGroups: any[] = [];
  public allPermissionGroups: any[] = [];
  public formGroup: FormGroup;
  private editedRowIndex: number;
  public rolePermissionsPopup: boolean = false;
  public editRolePermissionsPopup: boolean = false;
  public addRolePermissionsPopup: boolean = false;
  public removeConfirmationSubject: Subject<boolean> = new Subject<boolean>();
  public editMode: boolean = false;
  public addMode: boolean = false;
  public currentDataItemId: any;
  private rowsSelected: number[] = [];
  public allRowsSelected: boolean = false;
  public filteredPermissionGroups: any[] = [];
  public modules: any[] = [];
  public isSingleTenant = false;

  constructor(private formBuilder: FormBuilder, private roleService: RolesService, private messageService: MessageService,
     private licenseService: LicenseService, private sessionService: SessionService, private tenantService: TenantService, private userService: UserService) {
    this.createFormGroup = this.createFormGroup.bind(this);
    this.removeConfirmation = this.removeConfirmation.bind(this);
  }

  async ngOnInit() {
    let isSingleTenant = await this.licenseService.getIsSingleTenant().toPromise();
    
    let user = this.sessionService.getUser();
    if (isSingleTenant.isSingleTenant) {
      let res = await this.tenantService.getAllLicesesAssigneedToTenant(Guid.empty).toPromise();
      if (res) {
        let permissionGroups = [];
        for(let i = 0; i < res.length; i++) {
          let result = await this.tenantService.getAllPermissionGroupsAssignToLicense(res[i].id).toPromise() as any;
          for(let j = 0; j < result.length; j++) {
            if (permissionGroups.length === 0 || !permissionGroups.find(permissionGroup => permissionGroup.id === result[j].id)) {
              permissionGroups.push(result[j]);
            }
          }
        }
        this.sessionService.setUser({...user, permissionGroups: permissionGroups} as any);
      }
    }
    
    let licensePermissionGroups = this.sessionService.getUser().permissionGroups;
    this.roleService.getAllPermissionGroups().subscribe((res: any[]) => {
      this.allPermissionGroups = res.map(permission => {
        return {...permission, disabled: (permission.default || (isSingleTenant.isSingleTenant && 
          !licensePermissionGroups.find(x => x.id === permission.id)))};
      });
      this.setModulePermissions(this.allPermissionGroups);
    });

    this.getAllRoles();
  }

  public setModulePermissions(res){
    this.modules = [];
    res.map(row =>{
      let currentModule = this.modules.find(line =>line.title ===row.module);
      if (!currentModule)
        return this.modules.push({title: row.module, permissions: [].concat({...row, isChecked: false})});
      this.modules = this.modules.map(modulePermssion =>{
        if (modulePermssion.title !== row.module)
          return modulePermssion;
        return {...modulePermssion, permissions: modulePermssion.permissions.concat({...row, isChecked: false})}
      });
      this.modules = this.modules.filter(mod => mod.title !== 'AA Product Builder');
    });
  }

  private baseImageUrl: string = "https://demos.telerik.com/kendo-ui/content/web/panelbar/";

  private imageUrl(imageName: string) :string {
      return this.baseImageUrl + imageName + ".jpg";
  }

  public createFormGroup(args: any): FormGroup {
    const item = args.isNew ? new Role() : args.dataItem;

    this.formGroup = this.formBuilder.group({
      'id': [item.id],
      'name': [item.name, Validators.required],
      'description': [item.description, Validators.required],
      'permissionGroups': [item.permissionGroups]
    });

    return this.formGroup;
  }

  public confirmRemove(shouldRemove: boolean): void {
    this.roleService.deleteRole(this.itemToRemove.id).subscribe(res => {
        this.messageService.sendMessage({message: 'Role Deleted!', title: 'Action', type: 'info'});
        this.removeConfirmationSubject.next(shouldRemove);
      },
      err => {
        this.messageService.sendMessage({message: err.error.error, title: 'Action', type: 'warning'});
        this.getAllRoles();
        return;
      });
    this.itemToRemove = null;
  }

  public removeConfirmation(dataItem): Subject<boolean> {
    this.itemToRemove = dataItem;
    return this.removeConfirmationSubject;
  }

  public addHandler({sender}) {
    this.filteredPermissionGroups = this.allPermissionGroups.map(col => (
      {
        id: col.id,
        name: col.name,
        isChecked: 0
      }));
    this.addMode = true;
    this.formGroup = new FormGroup({
      'id': new FormControl(),
      'name': new FormControl('', Validators.required),
      'description': new FormControl('', Validators.required),
      'permissionGroups': new FormControl([])
    });
    sender.addRow(this.formGroup);
  }

  public selectAllRowsByModule(mod, event){
    this.modules.forEach(element => {
      if (element.title === mod.title){
        element.allRowsSelected = event.target.checked;
        element.expanded= true;
        element.permissions.forEach(permission => {
          if (!permission.default && !permission.disabled)
            permission.isChecked = event.target.checked;
        });
      }
    });
  }

  public async editHandler({sender, rowIndex, dataItem}) {
    let isSingleTenant = await this.licenseService.getIsSingleTenant().toPromise();
    this.isSingleTenant = isSingleTenant.isSingleTenant;
    let res, roleRes;
    if (isSingleTenant.isSingleTenant) {
      res = this.sessionService.getUser().permissionGroups;
      roleRes = await this.roleService.getAllPermissionGroupsAssignToRole(dataItem.id).toPromise();
    } else {
      res = await this.roleService.getAllPermissionGroupsAssignToRole(dataItem.id).toPromise();
    }
    let newPermissions = res.filter(x => !this.allPermissionGroups.find(allP => allP.id === x.id)).concat(this.allPermissionGroups);
    this.setModulePermissions(newPermissions);
    this.modules = this.modules.map(modulePermission =>{
      let newPermission = modulePermission.permissions.map(permission =>{
        const isChecked = (res.find(x => permission.id === x.id) && (!isSingleTenant.isSingleTenant || roleRes.find(x => x.id === permission.id)));
        return {id: permission.id, name: permission.name, default: permission.default, 
          isChecked: isChecked, disabled: (permission.default || (isSingleTenant.isSingleTenant && !res.find(x => permission.id === x.id)))}
      });
      return {...modulePermission, permissions: newPermission, allRowsSelected: newPermission.find(rowPermission => !rowPermission.isChecked) ? false : true};
    });
    const userPermissionGrs = res;
    this.editMode = true;
    this.formGroup = new FormGroup({
      'id': new FormControl(dataItem.id),
      'name': new FormControl(dataItem.name, Validators.required),
      'description': new FormControl(dataItem.description, Validators.required),
      'permissionGroups ': new FormControl(userPermissionGrs)
    });
    this.editedRowIndex = rowIndex;
    this.currentDataItemId = dataItem.id;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({sender, rowIndex}) {
    this.editMode = false;
    this.closeEditor(sender, rowIndex);
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  protected saveHandler({sender, rowIndex, formGroup, isNew}) {
    if (formGroup.value.name == '' || formGroup.value.description == '') {
      this.messageService.sendMessage({message: 'Please check input fields!', title: 'Action', type: 'warning'});
      return;
    }
    if (!isNew) {
      let permissionIds = [];
      this.modules.map(row =>{
        permissionIds = permissionIds.concat(row.permissions);
      });
      permissionIds = permissionIds.filter((x: any) =>x.isChecked && !x.default).map(res =>  {
        return {id: res.id} });
      formGroup.value.permissionGroups = permissionIds;
      this.roleService.updateRole(formGroup.value.id, formGroup.value.name, formGroup.value.description, permissionIds).subscribe(
        res => {
          sender.closeRow(rowIndex);
          this.editMode = false;
          this.messageService.sendMessage({message: 'Role updated!', title: 'Action', type: 'info'});
          this.getAllRoles();
          this.setModulePermissions(this.allPermissionGroups);
        },
        err => {
          this.messageService.sendMessage({message: err.error.error, title: 'Action', type: 'warning'});
          this.editMode = false;
          this.getAllRoles();
          return;
        });
    } else {
      let permissionIds = [];
      this.modules.map(row =>{
        permissionIds = permissionIds.concat(row.permissions);
      });
      permissionIds = permissionIds.filter((x: any) =>x.isChecked && !x.default).map(res =>  {
        return {id: res.id} });
      this.roleService.addRole(formGroup.value.name, formGroup.value.description, permissionIds).subscribe(
        res => {
          sender.closeRow(rowIndex);
          this.editMode = false;
          this.messageService.sendMessage({message: 'Role added!', title: 'Action', type: 'info'});
          this.getAllRoles();
          this.setModulePermissions(this.allPermissionGroups);
        },
        err => {

          this.messageService.sendMessage({message: err.error.error, title: 'Action', type: 'warning'});
          this.editMode = false;
          this.getAllRoles();
          return;
        });
    }

  }

  public cancelPermissions(){
    this.setModulePermissions(this.allPermissionGroups);
  }

  async viewPermissions(id) {
    // PermissionGroup/GetAllPermissionGroups  endpoint
    this.rolePermissionsPopup = true;
    let isSingleTenant = await this.licenseService.getIsSingleTenant().toPromise();
    let res;
    if (isSingleTenant.isSingleTenant) {
      res = this.sessionService.getUser().permissionGroups;
      let roleRes = await this.roleService.getAllPermissionGroupsAssignToRole(id).toPromise() as any;
      res = res.filter(x => roleRes.find(el => el.id === x.id));
    } else {
      res = await this.roleService.getAllPermissionGroupsAssignToRole(id).toPromise();
    }
    this.userPermissionGroups = res;
    this.setModulePermissions(res);
  }

  public addPermissions() {
    this.addRolePermissionsPopup = true;
  }

  public editPermissions(id) {
    this.editRolePermissionsPopup = true;
  }

  private selectAllRows(e): void {
    if (e.target.checked) {
      this.allRowsSelected = true;
      this.filteredPermissionGroups = this.allPermissionGroups.map(col => (
        {
          id: col.id,
          name: col.name,
          isChecked: 1
        }));
    } else {
      this.allRowsSelected = false;
      this.filteredPermissionGroups = this.allPermissionGroups.map(col => (
        {
          id: col.id,
          name: col.name,
          isChecked: 0
        }));
    }
  }

  private rowsSelectedKeys(context: RowArgs): number {
    return context.dataItem.id;
  }

  public checkValue(id, event: any, module) {
    this.modules.forEach(element => {
      if (element.title === module.title){
        element.permissions.forEach(permission =>{
          if(permission.id === id)
          permission.isChecked = event.target.checked;
        });
        element.allRowsSelected = element.permissions.find(row => !row.isChecked) ? false : true;
      }
    });
  }


  public getAllRoles() {
    this.roleService.getAllRoles().subscribe(res => {
      this.roles = res;
    });
  }

  public onStateChange(state: State) {
    this.getAllRoles();
  }

}
