import { LicenseService } from './../../../core/services/license.service';
import {Component, OnInit} from '@angular/core';
import {tenants} from '../tenant-list/tenants';
import { TenantService } from '../../services/Tenant.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: "app-main-body",
  templateUrl: "./main-body.component.html",
  styleUrls: ["./main-body.component.css"]
})
export class MainBodyComponent implements OnInit {
  public deleteTenantId: any;
  public deleteConfirm: false;
  public opened = false;
  public tenants: any = [];
  public isSingleTenant = false;

  constructor(
    private tenantService: TenantService,
    private _sanitizer: DomSanitizer,
    private licenseService: LicenseService,
    private router: Router
  ) {}

  async ngOnInit() {
    const result = await this.getIsSingleTenant();
    if ("isSingleTenant" in result && result.isSingleTenant) {
      this.isSingleTenant = result.isSingleTenant;
      this.router.navigateByUrl("/new-user");
      return;
    }

    this.tenantService.getAllTenants().subscribe(res => {
      this.tenants = res;
      this.tenants.forEach(tenant => {
        tenant.imageUrl = "";
        if (tenant.image) {
          tenant.image = atob(tenant.image);
        }
      });
    });
  }

  viewDetails(tenantData) {}

  async getIsSingleTenant() {
    return this.licenseService.getIsSingleTenant().toPromise();
  }
}
