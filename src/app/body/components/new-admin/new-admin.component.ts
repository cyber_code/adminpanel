import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MessageService} from '../../../core/services/message.service';
import {UserService} from '../../services/User.service';
import {UserRoleService} from '../../services/UserRole.Service';
import {TenantService} from '../../services/Tenant.service';
import {AdminPanelUserService} from '../../services/AdminPanelUser.service';

@Component({
  selector: 'app-new-admin',
  templateUrl: './new-admin.component.html',
  styleUrls: ['./new-admin.component.css']
})
export class NewAdminComponent implements OnInit {

  newUser: FormGroup;
  loading = false;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private adminPanelUserService: AdminPanelUserService) {}

  ngOnInit() {
    this.newUser = this.formBuilder.group(
      {
        fullName: ['', Validators.required],
        userName: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        phone: ['', Validators.required],
        password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(10)]],
        confirmPassword: ['', Validators.required]
      },
      {validator: this.checkPasswords}
    );

  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : {notSame: true};
  }

  get f() {
    return this.newUser.controls;
  }

  onSubmit() {
    this.loading = true;
    this.adminPanelUserService.addAdminPanelUser(
      this.f.fullName.value,
      this.f.userName.value,
      this.f.email.value,
      this.f.phone.value,
      this.f.password.value,
      this.f.confirmPassword.value
    ).subscribe(
      data => {
        this.messageService.sendMessage({
          message: data['message'],
          title: 'Success!',
          type: 'info'
        });
        this.newUser.reset();
        this.loading = false;
        this.router.navigateByUrl('/admin-list');
      },
      err => {
        this.messageService.sendMessage({
          message: err.error.error,
          title: 'Registration Error',
          type: 'danger'
        });
        this.loading = false;
      });
  }
}
