import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {UserService} from '../../services/User.service';
import {TenantService} from '../../services/Tenant.service';
import {MessageService} from '../../../core/services/message.service';
import {UserRoleService} from '../../services/UserRole.Service';
import {User} from '../../models/user';
import {AdminPanelUserService} from '../../services/AdminPanelUser.service';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent implements OnInit {
  selectedUserId: any;
  public itemToRemove: any;
  resetOpened: boolean = false;
  removeOpened: boolean = false;
  public users: any[] = [];
  public formGroup: FormGroup;
  private editedRowIndex: number;
  public removeConfirmationSubject: Subject<boolean> = new Subject<boolean>();
  public tenantList: any[] = [];
  public allRoles: any[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private messageService: MessageService,
    private adminPanelUserService: AdminPanelUserService
  ) {
    this.createFormGroup = this.createFormGroup.bind(this);
    this.removeConfirmation = this.removeConfirmation.bind(this);
  }

  ngOnInit() {

    this.getAdminUsers();

  }

  public createFormGroup(args: any): FormGroup {
    let item = args.isNew ? new User() : args.dataItem;
    this.formGroup = this.formBuilder.group({
      id: [item.id, Validators.required],
      fullName: [item.fullName, Validators.required],
      userName: [item.userName, Validators.required],
      phone: [item.phone],
      email: [item.email]
    });

    return this.formGroup;
  }

  resetPassword(userId) {
    this.selectedUserId = userId;
    this.resetOpened = true;
  }

  confirmReset(userId) {
    this.resetOpened = false;
    this.adminPanelUserService.resetPassword(userId).subscribe(res => {
        this.messageService.sendMessage({message: 'Password Reset Successfully!', title: 'Action', type: 'info'});
      },
      err => {
        this.messageService.sendMessage({message: err.error.error, title: 'Action', type: 'warning'});
        this.getAdminUsers();
        return;
      });
  }

  public confirmRemove(shouldRemove: boolean): void {
    this.adminPanelUserService.deleteAdminPanelUser(this.itemToRemove.id).subscribe(res => {
        this.messageService.sendMessage({message: 'User Deleted!', title: 'Action', type: 'info'});
      },
      err => {
        this.messageService.sendMessage({message: err.error.error, title: 'Action', type: 'warning'});
        this.getAdminUsers();
        return;
      });
    this.removeConfirmationSubject.next(shouldRemove);
    this.itemToRemove = null;
  }

  public removeConfirmation(dataItem): Subject<boolean> {
    this.itemToRemove = dataItem;
    return this.removeConfirmationSubject;
  }

  public editHandler({sender, rowIndex, dataItem}) {
    this.formGroup = new FormGroup({
      id: new FormControl(dataItem.id, Validators.required),
      fullName: new FormControl(dataItem.fullName, Validators.required),
      userName: new FormControl(dataItem.userName, Validators.required),
      phone: new FormControl(dataItem.phone),
      email: new FormControl(dataItem.email)
    });

    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({sender, rowIndex}) {
    this.closeEditor(sender, rowIndex);
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  protected saveHandler({sender, rowIndex, formGroup, isNew}) {
    let formValues = formGroup.value;
    this.adminPanelUserService.updateAdminPanelUser(formValues.id, formValues.userName, formValues.email, formValues.phone, formValues.fullName).subscribe(
      res => {
        sender.closeRow(rowIndex);
        this.getAdminUsers();
        this.messageService.sendMessage({message: 'User Updated!', title: 'Action', type: 'info'});
      },
      err => {
        this.messageService.sendMessage({message: err.error.error, title: 'Action', type: 'warning'});
        this.getAdminUsers();
        return;
      });
  }

  public getAdminUsers() {
    this.adminPanelUserService.getAllAdminPanelUsers().subscribe(res => {
      this.users = res;
    });
  }
}
