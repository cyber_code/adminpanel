import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MessageService} from '../../../core/services/message.service';
import {UserService} from '../../services/User.service';
import {TenantService} from '../../services/Tenant.service';
import {tenants} from '../tenant-list/tenants';
import {roles} from '../user-roles/roles';
import {UserRoleService} from '../../services/UserRole.Service';
import { SessionService } from 'src/app/core/services/session.service';
import { LicenseService } from 'src/app/core/services/license.service';
import { Guid } from 'src/app/shared/guid';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  newUser: FormGroup;
  loading = false;
  submitted = false;
  tenantsList: any[] = [];
  selectedItems: any[] = [];
  dropdownList: any[] = [];
  dropdownSettings = {};

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private userService: UserService,
    private userRoleService: UserRoleService,
    private tenantService: TenantService,
    private sessionService: SessionService,
    private licenseService: LicenseService
  ) {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'ID',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3
    };
  }

  async ngOnInit() {
    this.newUser = this.formBuilder.group(
      {
        fullName: ['', Validators.required],
        userName: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        phone: ['', Validators.required],
        roles: [[], Validators.required],
        password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(10)]],
        confirmPassword: ['', Validators.required],
        tenantId: ['', Validators.required]
      },
      {validator: this.checkPasswords}
    );
    this.userRoleService.getAllRoles().subscribe(data => {
      this.dropdownList = data;
      this.dropdownList.forEach(item => {
        item.itemName = item.name;
      });
    });
    this.tenantService.getAllTenants().subscribe(data => {
      this.tenantsList = data;
    });
  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.confirmPassword.value;

    return pass === confirmPass ? null : {notSame: true};
  }

  get f() {
    return this.newUser.controls;
  }

  onItemSelect(item: any) {
  }

  OnItemDeSelect(item: any) {
  }

  onSelectAll(items: any) {

  }

  onDeSelectAll(items: any) {
  }

  onSubmit() {
    this.loading = true;
    this.userService.addUser(
      this.f.fullName.value,
      this.f.userName.value,
      this.f.email.value,
      this.f.phone.value,
      this.f.password.value,
      this.f.confirmPassword.value,
      this.f.roles.value,
      this.f.tenantId.value).subscribe(
      data => {
        this.router.navigateByUrl('/user-list');
      },
      err => {
        this.messageService.sendMessage({
          message: err.error.error,
          title: 'Registration Error',
          type: 'danger'
        });
        this.loading = false;
      });
  }

}
