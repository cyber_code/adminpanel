import { Component, OnInit } from '@angular/core';
import { TenantService } from '../../services/Tenant.service';
import { tenants } from './tenants';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { LicenseService } from 'src/app/core/services/license.service';
@Component({
  selector: "app-tenant-list",
  templateUrl: "./tenant-list.component.html",
  styleUrls: ["./tenant-list.component.css"]
})
export class TenantListComponent implements OnInit {
  deleteTenantId: any;
  deleteConfirm: false;
  public opened = false;
  public tenants: any = [];
  public isSingleTenant = false;

  constructor(
    private tenantService: TenantService,
    private _sanitizer: DomSanitizer,
    private router: Router,
    private licenseService: LicenseService
  ) {}

  async ngOnInit() {
    const result = await this.getIsSingleTenant();
    if ("isSingleTenant" in result && result.isSingleTenant) {
      this.isSingleTenant = result.isSingleTenant;
      this.router.navigateByUrl("/new-user");
      return;
    }

    this.tenantService.getAllTenants().subscribe(res => {
      this.tenants = res;
      this.tenants.forEach(tenant => {
        tenant.imageUrl = "";
        if (tenant.image) {
          tenant.image = atob(tenant.image);
        }
      });
    });
  }

  async getIsSingleTenant() {
    return this.licenseService.getIsSingleTenant().toPromise();
  }

  viewDetails(tenantId) {}

  onScroll() {}
}
