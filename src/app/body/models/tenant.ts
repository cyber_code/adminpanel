export class User {
  public id: number;
  public username: string;
  public email: string;
  public about: string;
  public license: number;
}
