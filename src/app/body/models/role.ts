export class Role {
  public ID: number;
  public name: string;
  public processIntelligence: boolean;
  public testDataIntelligence: boolean;
  public testCaseModule: boolean;
  public insightsModule: boolean;
  public productModule: boolean;
}
