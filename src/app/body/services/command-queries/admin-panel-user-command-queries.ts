import {ApiAction} from '../../../core/models/command-queries/api-action';

export class GetAllAdminPanelUsers extends ApiAction {
  constructor() {
    super('GetAllAdminPanelUsers');
  }
}
//
// export class GetUser extends ApiAction {
//   constructor(userId: string) {
//     super('GetUser');
//     this.userId = userId;
//   }
//
//   userId: string;
// }

export class AddAdminPanelUser extends ApiAction {
  fullName: string;
  userName: string;
  email: string;
  phone: string;
  password: string;
  confirmPassword: string;

  constructor(fullName: string, userName: string, email: string, phone: string, password: string, confirmPassword: string) {
    super('AddAdminPanelUser');
    this.fullName = fullName;
    this.userName = userName;
    this.email = email;
    this.phone = phone;
    this.password = password;
    this.confirmPassword = confirmPassword;
  }
}

export class UpdateAdminPanelUser extends ApiAction {
  id: string;
  userName: string;
  email: string;
  phone: string;
  fullName: string;

  constructor(id: string, userName: string, email: string, phone: string, fullName: string,) {
    super('UpdateAdminPanelUser');
    this.id = id;
    this.userName = userName;
    this.email = email;
    this.phone = phone;
    this.fullName = fullName;
  }
}

export class DeleteAdminPanelUser extends ApiAction {
  userId: string;
  constructor(userId: string) {
    super('DeleteAdminPanelUser');
    this.userId = userId;
  }
}

export class ResetPassword extends ApiAction {
  userId: string;
  constructor(userId: string) {
    super('ResetPassword');
    this.userId = userId;
  }
}

export class ChangePassword extends ApiAction {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;

  constructor(oldPassword: string, newPassword: string, confirmPassword: string) {
    super('ChangePassword');
    this.oldPassword = oldPassword;
    this.newPassword = newPassword;
    this.confirmPassword = confirmPassword;
  }
}

