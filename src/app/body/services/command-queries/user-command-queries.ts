import {ApiAction} from '../../../core/models/command-queries/api-action';

export class GetAllUsers extends ApiAction {
  constructor() {
    super('GetAllUsers');
  }

  userId: string;
}

export class GetUser extends ApiAction {
  constructor(userId: string) {
    super('GetUserById');
    this.userId = userId;
  }

  userId: string;
}

export class AddUser extends ApiAction {
  fullName: string;
  userName: string;
  email: string;
  phone: string;
  password: string;
  confirmPassword: string;
  roles: string;
  tenantId: string;

  constructor(fullName: string, userName: string, email: string, phone: string, password: string, confirmPassword: string, roles: string, tenantId: string) {
    super('AddUser');
    this.fullName = fullName;
    this.userName = userName;
    this.email = email;
    this.password = password;
    this.confirmPassword = confirmPassword;
    this.roles = roles;
    this.tenantId = tenantId;
  }
}

export class UpdateUser extends ApiAction {
  id: string;
  userName: string;
  email: string;
  phone: string;
  fullName: string;
  tenantId: string;
  roles: any;

  constructor(id: string, userName: string, email: string, phone: string, fullName: string, tenantId: string, roles) {
    super('UpdateUser');
    this.id = id;
    this.userName = userName;
    this.email = email;
    this.phone = phone;
    this.fullName = fullName;
    this.tenantId = tenantId;
    this.roles = roles;
  }
}

export class DeleteUser extends ApiAction {
  userId: string;

  constructor(userId: string) {
    super('DeleteUser');
    this.userId = userId;
  }
}

export class ResetPassword extends ApiAction {
  userId: string;
  constructor(userId: string) {
    super('ResetPassword');
    this.userId = userId;
  }
}
