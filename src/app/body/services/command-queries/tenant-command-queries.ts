import { ApiAction } from "../../../core/models/command-queries/api-action";

export class GetAllTenants extends ApiAction {
  constructor() {
    super("GetAllTenants");
  }
}

export class GetTenantById extends ApiAction {
  constructor(id: string) {
    super("GetTenantById");
    this.tenantId = id;
  }

  tenantId: string;
}

export class AddTenant extends ApiAction {
  name: string;
  userName: string;
  email: string;
  image: string;
  password: string;
  confirmPassword: string;
  licenseIds: number[];
  address: string;
  address2: string;
  city: string;
  state: string;
  zip: string;
  description: string;
  roleId: string;

  constructor(
    name: string,
    userName: string,
    email: string,
    image: string,
    password: string,
    confirmPassword: string,
    licenseIds: number[],
    address: string,
    address2: string,
    city: string,
    state: string,
    zip: string,
    description: string,
    roleId: string
  ) {
    super("AddTenant");
    this.name = name;
    this.userName = userName;
    this.email = email;
    this.image = image;
    this.password = password;
    this.confirmPassword = confirmPassword;
    this.licenseIds = licenseIds;
    this.address = address;
    this.address2 = address2;
    this.city = city;
    this.state = state;
    this.zip = zip;
    this.description = description;
    this.roleId = roleId;
  }
}

export class UpdateTenant extends ApiAction {
  id: string;
  name: string;
  email: string;
  image: string;
  licenseIds: number[];
  address: string;
  address2: string;
  city: string;
  state: string;
  zip: string;
  description: string;

  constructor(
    id: string,
    name: string,
    email: string,
    image: string,
    licenseIds: number[],
    address: string,
    address2: string,
    city: string,
    state: string,
    zip: string,
    description: string
  ) {
    super("UpdateTenant");
    this.id = id;
    this.name = name;
    this.email = email;
    this.image = image;
    this.licenseIds = licenseIds;
    this.address = address;
    this.address2 = address2;
    this.city = city;
    this.state = zip;
    this.zip = email;
    this.description = description;
  }
}

export class GetAllLicesesAssigneedToTenant extends ApiAction {
  tenantId: string;

  constructor(tenantId: string) {
    super('GetAllLicesesAssigneedToTenant');
    this.tenantId = tenantId;
  }
}

export class GetAllPermissionGroupsAssignToLicense extends ApiAction {
  licenseid: string;

  constructor(licenseId: string) {
    super('GetAllPermissionGroupsAssignToLicense');
    this.licenseid = licenseId;
  }
}

export class DeleteTenant extends ApiAction {
  tenantId: string;

  constructor(tenantId: string) {
    super("DeleteTenant");
    this.tenantId = tenantId;
  }
}

export class ImageUpload extends ApiAction {
  tenantId: string;

  constructor(tenantId: string) {
    super("ImageUpload");
    this.tenantId = tenantId;
  }
}

export class ResetPassword extends ApiAction {
  tenantId: string;

  constructor(tenantId: string) {
    super("ResetPassword");
    this.tenantId = tenantId;
  }
}

export class GetAllLiceses extends ApiAction {
  constructor() {
    super("GetAllLiceses");
  }
}

export class GetUserByTenantId extends ApiAction {
  tenantId: string;
  constructor(tenantId: string) {
    super("GetUserByTenantId");
    this.tenantId = tenantId;
  }
}
