import {ApiAction} from '../../../core/models/command-queries/api-action';

export class GetAllRoles extends ApiAction {
  constructor() {
    super('GetAllRoles');
  }
}
export class GetAllPermissionGroups extends ApiAction {
  constructor() {
    super('GetAllPermissionGroups');
  }
}

export class GetAllPermissionGroupsAssignToRole extends ApiAction {
  roleId: number;
  constructor(roleId: number) {
    super('GetAllPermissionGroupsAssignToRole');
    this.roleId = roleId;
  }
}

export class AddRole extends ApiAction {
  name: string;
  description: string;
  permissionGroups: any[];

  constructor(name: string, description: string, permissionGroups: any[]) {
    super('AddRole');
    this.name = name;
    this.description = description;
    this.permissionGroups = permissionGroups;
  }
}


export class UpdateRole extends ApiAction {
  id:number;
  name: string;
  description: string;
  permissionGroups: any[];

  constructor(id: number, name: string, description: string, permissionGroups: any[]) {
    super('UpdateRole');
    this.id = id;
    this.name = name;
    this.description = description;
    this.permissionGroups = permissionGroups;
  }
}



export class AddUserRole extends ApiAction {
  id: number;
  name: string;
  processIntelligence: boolean;
  testDataIntelligence: boolean;
  testCaseModule: boolean;
  insightsModule: boolean;
  productModule: boolean;

  constructor(id: number,
              name: string,
              processIntelligence: boolean,
              testDataIntelligence: boolean,
              testCaseModule: boolean,
              insightsModule: boolean,
              productModule: boolean) {
    super('AddUserRole');
    this.id = id;
    this.name = name;
    this.processIntelligence = processIntelligence;
    this.testDataIntelligence = testDataIntelligence;
    this.testCaseModule = testCaseModule;
    this.insightsModule = insightsModule;
    this.productModule = productModule;
  }
}

export class UpdateUserRole extends ApiAction {
  id: number;
  name: string;
  processIntelligence: boolean;
  testDataIntelligence: boolean;
  testCaseModule: boolean;
  insightsModule: boolean;
  productModule: boolean;

  constructor(id: number,
              name: string,
              processIntelligence?: boolean,
              testDataIntelligence?: boolean,
              testCaseModule?: boolean,
              insightsModule?: boolean,
              productModule?: boolean) {
    super('UpdateUserRole');
    this.id = id;
    this.name = name;
    this.processIntelligence = processIntelligence;
    this.testDataIntelligence = testDataIntelligence;
    this.testCaseModule = testCaseModule;
    this.insightsModule = insightsModule;
    this.productModule = productModule;
  }
}

export class DeleteRole extends ApiAction {
  roleId: number;
  constructor(roleId: number) {
    super('DeleteRole');
    this.roleId = roleId;
  }
}

export class DeleteUserRole extends ApiAction {
  id: string;

  constructor(id: string) {
    super('DeleteUserRole');
    this.id = id;
  }
}
