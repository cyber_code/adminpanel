import {SessionService} from './../../core/services/session.service';
import {Observable} from 'rxjs';
import {HttpExecutorService} from 'src/app/core/services/http-executor.service';
import {CommandMethod} from 'src/app/shared/command-method';
import {ConfigurationService} from 'src/app/core/services/configuration.service';
import {Injectable} from '@angular/core';
import {
  AddRole,
  DeleteRole,
  GetAllRoles,
  GetAllPermissionGroupsAssignToRole,
  GetAllPermissionGroups,
  UpdateRole
} from './command-queries/role-command-queries';
import {Role} from '../models/role';

@Injectable()
export class RolesService {
  private serviceUrl: string;
  private permissionsUrl: string;

  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService,
    private sessionService: SessionService
  ) {
    this.serviceUrl = `${configurationService.serverSettings.identityUrl}Role/`;
    this.permissionsUrl = `${configurationService.serverSettings.identityUrl}PermissionGroup/`;
  }

  getAllRoles(): Observable<[]> {
    return this.httpExecutor.executeQuery<[]>(this.serviceUrl, new GetAllRoles());
  }

  getAllPermissionGroupsAssignToRole(roleId: number) {
    return this.httpExecutor.executeQuery<[]>(this.serviceUrl, new GetAllPermissionGroupsAssignToRole(roleId));
  }

  getAllPermissionGroups() {
    return this.httpExecutor.executeQuery<[]>(this.permissionsUrl, new GetAllPermissionGroups());
  }

  addRole(name: string, description: string, permissionGroups: any[]): Observable<any[]> {
    return this.httpExecutor.executeCommand<any[]>(
      this.serviceUrl, new AddRole(name, description, permissionGroups), CommandMethod.POST);
  }

  updateRole(id: number, name: string, description: string, permissionGroups: any[]): Observable<any[]> {
    return this.httpExecutor.executeCommand<any>(
      this.serviceUrl, new UpdateRole(id, name, description, permissionGroups), CommandMethod.PUT);
  }

  deleteRole(roleId: number): Observable<Role[]> {
    return this.httpExecutor.executeCommand<any[]>(this.serviceUrl, new DeleteRole(roleId), CommandMethod.DELETE);
  }
}
