import {SessionService} from '../../core/services/session.service';
import {Observable} from 'rxjs';
import {HttpExecutorService} from 'src/app/core/services/http-executor.service';
import {CommandMethod} from 'src/app/shared/command-method';
import {ConfigurationService} from 'src/app/core/services/configuration.service';
import {Injectable} from '@angular/core';
import {
  AddAdminPanelUser,
  UpdateAdminPanelUser,
  ChangePassword,
  GetAllAdminPanelUsers,
  DeleteAdminPanelUser,
  ResetPassword
} from './command-queries/admin-panel-user-command-queries';


@Injectable()
export class AdminPanelUserService {
  private userUrl: string;

  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService,
    private sessionService: SessionService
  ) {
    this.userUrl = `${configurationService.serverSettings.identityUrl}AdminPanelUser/`;
  }

  getAllAdminPanelUsers(): Observable<[]> {
    return this.httpExecutor.executeQuery<[]>(this.userUrl, new GetAllAdminPanelUsers());
  }

  //
  //
  // getUser(id: string): Observable<[]> {
  //   return this.httpExecutor.executeQuery<[]>(this.userUrl, new GetUser(id));
  // }

  addAdminPanelUser(fullName: string, userName: string, email: string, phone: string, password: string, confirmPassword: string): Observable<any[]> {
    return this.httpExecutor.executeCommand<any[]>(this.userUrl, new AddAdminPanelUser(fullName, userName, email, phone, password, confirmPassword), CommandMethod.POST);
  }

  updateAdminPanelUser(id: string, userName: string, email: string, phone: string, fullName: string): Observable<any[]> {
    return this.httpExecutor.executeCommand<any>(this.userUrl, new UpdateAdminPanelUser(id, userName, email, phone, fullName), CommandMethod.PUT);
  }


  changePassword(oldPassword: string, newPassword: string, confirmPassword: string): Observable<any[]> {
    return this.httpExecutor.executeCommand<any[]>(this.userUrl, new ChangePassword(oldPassword, newPassword, confirmPassword), CommandMethod.POST);
  }

  deleteAdminPanelUser(userId: string): Observable<any[]> {
    return this.httpExecutor.executeCommand<any[]>(this.userUrl, new DeleteAdminPanelUser(userId), CommandMethod.DELETE);
  }

  resetPassword(userId: string): Observable<any[]> {
    return this.httpExecutor.executeCommand<any[]>(this.userUrl, new ResetPassword(userId), CommandMethod.POST);
  }


}
