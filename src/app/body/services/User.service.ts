import {SessionService} from './../../core/services/session.service';
import {Observable} from 'rxjs';
import {HttpExecutorService} from 'src/app/core/services/http-executor.service';
import {CommandMethod} from 'src/app/shared/command-method';
import {ConfigurationService} from 'src/app/core/services/configuration.service';
import {
  GetAllUsers,
  AddUser,
  DeleteUser,
  GetUser,
  ResetPassword,
  UpdateUser
} from './command-queries/user-command-queries';
import {Injectable} from '@angular/core';
import {GetUserByTenantId} from './command-queries/tenant-command-queries';

@Injectable()
export class UserService {
  private userUrl: string;

  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService,
    private sessionService: SessionService
  ) {
    this.userUrl = `${configurationService.serverSettings.identityUrl}User/`;
  }

  getAllUsers(): Observable<[]> {
    return this.httpExecutor.executeQuery<[]>(this.userUrl, new GetAllUsers());
  }


  getUser(id: string): Observable<[]> {
    return this.httpExecutor.executeQuery<[]>(this.userUrl, new GetUser(id));
  }

  addUser(fullName: string, userName: string, email: string, phone: string, password: string, confirmPassword: string, roles: string, tenantId: string): Observable<any[]> {
    return this.httpExecutor.executeCommand<any[]>(this.userUrl, new AddUser(fullName, userName, email, phone, password, confirmPassword, roles, tenantId), CommandMethod.POST);
  }

  updateUser(id: string, userName: string, email: string, phone: string, fullName: string, tenantId: string, roles): Observable<any[]> {
    return this.httpExecutor.executeCommand<any>(this.userUrl, new UpdateUser(id, userName, email,phone,fullName, tenantId,roles), CommandMethod.PUT);
  }

  deleteUser(userId: string): Observable<any[]> {
    return this.httpExecutor.executeCommand<any[]>(this.userUrl, new DeleteUser(userId), CommandMethod.DELETE);
  }

  resetPassword(userId: string): Observable<any[]> {
    return this.httpExecutor.executeCommand<any[]>(this.userUrl, new ResetPassword(userId), CommandMethod.POST);
  }

  getUserByTenantId(tenantId) {
    return this.httpExecutor.executeCommand<any[]>(this.userUrl, new GetUserByTenantId(tenantId), CommandMethod.GET);
  }

}
