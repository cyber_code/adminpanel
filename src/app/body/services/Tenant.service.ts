import { SessionService } from "./../../core/services/session.service";
import { Observable } from "rxjs";
import { HttpExecutorService } from "src/app/core/services/http-executor.service";
import { CommandMethod } from "src/app/shared/command-method";
import { ConfigurationService } from "src/app/core/services/configuration.service";
import { Injectable } from "@angular/core";
import {
  AddTenant,
  DeleteTenant,
  GetTenantById,
  GetAllTenants,
  ImageUpload,
  UpdateTenant,
  ResetPassword,
  GetAllLiceses,
  GetUserByTenantId,
  GetAllLicesesAssigneedToTenant,
  GetAllPermissionGroupsAssignToLicense
} from "./command-queries/tenant-command-queries";

@Injectable()
export class TenantService {
  private tenantUrl: string;
  private licenseUrl: string;

  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService,
    private sessionService: SessionService
  ) {
    this.tenantUrl = `${configurationService.serverSettings.identityUrl}Tenant/`;
    this.licenseUrl = `${configurationService.serverSettings.identityUrl}License/`;
  }

  getAllTenants(): Observable<[]> {
    return this.httpExecutor.executeQuery<[]>(
      this.tenantUrl,
      new GetAllTenants()
    );
  }

  getTenantById(id: string): Observable<[]> {
    return this.httpExecutor.executeQuery<[]>(
      this.tenantUrl,
      new GetTenantById(id)
    );
  }

  addTenant(
    name: string,
    userName: string,
    email: string,
    image: string,
    password: string,
    confirmPassword: string,
    licenseIds: number[],
    address: string,
    address2: string,
    city: string,
    state: string,
    zip: string,
    description: string,
    roleId: string
  ): Observable<any[]> {
    return this.httpExecutor.executeCommand<any[]>(
      this.tenantUrl,
      new AddTenant(
        name,
        userName,
        email,
        image,
        password,
        confirmPassword,
        licenseIds,
        address,
        address2,
        city,
        state,
        zip,
        description,
        roleId
      ),
      CommandMethod.POST
    );
  }

  getAllLicesesAssigneedToTenant(tenantId: string): Observable<any> {
    return this.httpExecutor.executeQuery<any>(
      this.tenantUrl,
      new GetAllLicesesAssigneedToTenant(tenantId)
    );
  }

  updateTenant(
    id: string,
    name: string,
    email: string,
    image: string,
    licenseIds: number[],
    address: string,
    address2: string,
    city: string,
    state: string,
    zip: string,
    description: string
  ): Observable<any[]> {
    return this.httpExecutor.executeCommand<any>(
      this.tenantUrl,
      new UpdateTenant(
        id,
        name,
        email,
        image,
        licenseIds,
        address,
        address2,
        city,
        state,
        zip,
        description
      ),
      CommandMethod.PUT
    );
  }

  deleteTenant(tenantId: string): Observable<any[]> {
    return this.httpExecutor.executeCommand<any>(
      this.tenantUrl,
      new DeleteTenant(tenantId),
      CommandMethod.DELETE
    );
  }

  imageUpload(tenantId: string): Observable<any[]> {
    return this.httpExecutor.executeCommand<any[]>(
      this.tenantUrl,
      new ImageUpload(tenantId),
      CommandMethod.POST
    );
  }

  resetPassword(tenantId: string): Observable<any[]> {
    return this.httpExecutor.executeCommand<any[]>(
      this.tenantUrl,
      new ResetPassword(tenantId),
      CommandMethod.POST
    );
  }

  getAllLiceses(): Observable<any[]> {
    return this.httpExecutor.executeCommand<any[]>(
      this.licenseUrl,
      new GetAllLiceses(),
      CommandMethod.GET
    );
  }

  getAllPermissionGroupsAssignToLicense(licenseId) {
    return this.httpExecutor.executeQuery(
      this.licenseUrl,
      new GetAllPermissionGroupsAssignToLicense(licenseId)
    )
  }
}
