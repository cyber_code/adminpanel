import { SessionService } from './../../core/services/session.service';
import { Observable } from 'rxjs';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';
import { CommandMethod } from 'src/app/shared/command-method';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Injectable } from '@angular/core';
import {
  AddUserRole,
  DeleteUserRole,
  GetAllRoles,
  UpdateUserRole
} from './command-queries/role-command-queries';
import { Role } from '../models/role';

@Injectable()
export class UserRoleService {
  private serviceUrl: string;

  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService,
    private sessionService: SessionService
  ) {
    this.serviceUrl = `${configurationService.serverSettings.identityUrl}Role/`;
  }

  getAllRoles(): Observable<[]> {
    return this.httpExecutor.executeQuery<[]>(this.serviceUrl, new GetAllRoles());
  }

  addUserRole(
    id: number,
    name: string,
    processIntelligence: boolean,
    testDataIntelligence: boolean,
    testCaseModule: boolean,
    insightsModule: boolean,
    productModule: boolean
  ): Observable<Role[]> {
    return this.httpExecutor.executeCommand<any[]>(
      this.serviceUrl,
      new AddUserRole(
        id,
        name,
        processIntelligence,
        testDataIntelligence,
        testCaseModule,
        insightsModule,
        productModule
      ),
      CommandMethod.POST
    );
  }

  updateUserRole(
    id: number,
    name: string,
    processIntelligence?: boolean,
    testDataIntelligence?: boolean,
    testCaseModule?: boolean,
    insightsModule?: boolean,
    productModule?: boolean
  ): Observable<Role[]> {
    return this.httpExecutor.executeCommand<any>(
      this.serviceUrl,
      new UpdateUserRole(
        id,
        name,
        processIntelligence,
        testDataIntelligence,
        testCaseModule,
        insightsModule,
        productModule
      ),
      CommandMethod.PUT
    );
  }

  deleteUserRole(userId: string): Observable<Role[]> {
    return this.httpExecutor.executeCommand<any[]>(
      this.serviceUrl,
      new DeleteUserRole(userId),
      CommandMethod.DELETE
    );
  }
}
