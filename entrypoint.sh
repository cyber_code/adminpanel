#!/usr/bin/env sh

set -e

CONFIG=/usr/share/nginx/html/config.json
echo "{" > $CONFIG
echo "\"identityUrl\": \"$IDENTITYURL\", " >> $CONFIG
echo "\"apiUrl\": \"$APIURL\", " >> $CONFIG
echo "\"reportsUrl\": \"$REPORTSURL\", " >> $CONFIG
echo "\"hashId\": \"$HASHID\"" >> $CONFIG
echo "}" >> $CONFIG

exec "$@"